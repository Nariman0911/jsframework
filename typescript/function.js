function logInfo(name, age) {
    console.log("Info: " + name + ", " + age);
}
logInfo('Nariman', 29);
function calc(a, b) {
    if (typeof b === 'string')
        b = +b;
    return a + b;
}
console.log(calc(2, '5'));
