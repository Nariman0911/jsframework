let str: any = "Hello Typescript"
let num: number = 42
let isActive: boolean = false

let strArray: string[]= ['H', 'e', 'l']
let str2Array: Array<string>= ['H', 'e', 'l']
let numArray: Array<number> = [1, 2, 3]