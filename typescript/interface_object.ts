interface UserInterface {
    name: string
    age: number
    logInfo: () => void
    id?: any
}

const user: UserInterface = {
    name: 'Nariman',
    age: 29,
    logInfo() {
        console.log(this.name, + ' ' + this.age)
    }
}

