// const arr: Array<number> = [1, 2, 3, 4]

interface User{
    id: number
    name: string
    age: number
}

const users1: Observable<boolean> = [
    {id: 1, name: 'name1', age: 2},
    {id: 2, name: 'name2', age: 3}
]

const users: Array<User> = [
    {id: 1, name: 'name1', age: 2},
    {id: 2, name: 'name2', age: 3}
]

const users2: User[] = [
    {id: 1, name: 'name1', age: 2},
    {id: 2, name: 'name2', age: 3}
]