import {Component} from '@angular/core'

@Component({
    selector: 'app-post4',
    template: `
        <div class="post4">
            <h2>Post title</h2>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam eveniet nulla maiores mollitia neque sit, nisi voluptatum fuga quidem. Ab quis ratione, minima nihil quibusdam voluptates voluptatem iste ipsum animi.</p>
        </div>
    `,
    styles: [`
        .post4{
            padding: .5rem;
            border: 2px solid black;
        }
    `]
})

export class Post4Component {}